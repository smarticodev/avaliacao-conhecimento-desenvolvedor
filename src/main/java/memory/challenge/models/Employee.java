package memory.challenge.models;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import memory.challenge.utils.enums.EmployeesFunction;
import java.math.BigDecimal;
import java.time.LocalDate;

import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "uuid")
    private UUID id;

    private String name;

    @Column(name = "cpf", unique = true)
    private String cpf;

    private LocalDate admissionDate;

    private EmployeesFunction function;

    private BigDecimal remuneration;

    @OneToMany(mappedBy = "manager")
    private List<Employee> subordinates;

    @ManyToOne
    @JsonBackReference
    private Employee manager;


    public  Employee() {
    }

    public Employee(String id) {
        this.id = UUID.fromString(id);
    }
}







