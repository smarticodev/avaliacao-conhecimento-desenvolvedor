package memory.challenge.service;

import memory.challenge.models.Employee;
import memory.challenge.repository.EmployeeRepository;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.UUID;

@Component
public class EmployeeServiceImp implements EmployeeService{

    @Autowired
    EmployeeRepository repository = null;

    public List<Employee> list() {
        return repository.findAll();
    }

    public Employee create(Employee employee) {
        try {
            return repository.save(employee);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            return null;
        }
    }
}
