package memory.challenge.service;

import memory.challenge.models.Employee;
import memory.challenge.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeService {

    public List<Employee> list();

    public Employee create(Employee employee);

}
