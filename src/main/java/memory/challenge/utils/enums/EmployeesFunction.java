package memory.challenge.utils.enums;

public enum EmployeesFunction {
    FRONT_END,
    BACK_END,
    FULL_STACK,
    DATA_SCIENCE
}
